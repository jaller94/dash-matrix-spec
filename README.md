# Dash builder for Matrix Sepcification

Builds Matrix Specification as a [Dash docset](https://github.com/Kapeli/Dash-User-Contributions).

To be used with the offline documentation viewers [Dash](https://kapeli.com/dash) or [Zeal](https://zealdocs.org/).

## Use with Zeal

```sh
wget https://chrpaul.de/dash/Matrix-docset-latest.tgz
tar -xf Matrix-docset-latest.tgz -C ~/.local/share/Zeal/Zeal/docsets/
```

I don't want to host a self-updating feed. This docset has become an official Dash User Contribution.  
https://github.com/Kapeli/Dash-User-Contributions/issues/3750

## Where to find the docset?

The docset is built on Gitlab CI and is available as artifacts.

1. Go to a pipeline with the status "passed".
2. Click on the step "build".
3. Find the section "Job artifacts" in the sidebar and click on "Browse".
4. Download "Matrix.tgz".

## Known issues
* After Every link to the root page is broken. We have a hacky `sed` replacement workaround for this. 🩹
* The navigation links link to external pages and are mostly irrelevant for an offline specification. Search for `class="td-navbar-nav-scroll` and remove the `div`, if you want the navigation links removed.
