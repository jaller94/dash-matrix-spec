#!/bin/sh

# Fail on error
set -e

# Download Spec
if [ ! -d matrix-spec ]; then
  git clone --recurse-submodules --shallow-submodules --depth 1 --branch v1.12 https://github.com/matrix-org/matrix-spec.git;
fi
cd matrix-spec

# Turn links to Hugo "ref"s in content/**/*.md
#find content -type f -name "*.md" -exec sed --in-place -E "s/\((\/[^.)]*?)\/?(#.*?)\)/({{< ref \"\1\2\" >}})/g" {} +
# Workaround: POSIX sed does not support non-greedy expansions.
find content -type f -name "*.md" -exec sed --in-place -E "s/\((\/[^.)]*)\)/({{< ref \"\1\" >}})/g" {} +
find content -type f -name "*.md" -exec sed --in-place -E "s/\/#/#/g" {} +

# Install NodeJS dependencies
npm install

# Adapt config.toml
sed --in-place -E 's/canonifyURLs = true/canonifyURLs = false\nrelativeURLs = true\nuglyURLs = true\n/' config.toml

# Build the page
hugo.0.117.0

# Fix links to the root document in public/**/*.html
find public -type f -name "*.html" -exec sed --in-place -E "s/href=\"((.\/)?(..\/)*)\"/href=\"\1index.html\"/g" {} +

# Remove external font imports
# Otherwise, we fail the following CI check of https://github.com/Kapeli/Dash-User-Contributions
# "Detected online resource for: dashIndexFilePath"
find public -type f -name "*.css" -exec sed --in-place -E "s/@import \"https:[^\"]*\";//g" {} +
sed --in-place -E 's/@import "https:[^"]*";//g' config.toml

cd ..

sqlite3 Matrix.docset/Contents/Resources/docSet.dsidx < index.sql

mkdir "Matrix.docset/Contents/Resources/Documents/spec.matrix.org"
mv matrix-spec/public/* "Matrix.docset/Contents/Resources/Documents/spec.matrix.org"

# Try to build a reproducible archive.
tar --sort=name --mtime='@0' --format=gnu --owner=0 --group=0 --numeric-owner --exclude='.DS_Store' --exclude='.gitkeep' -cp Matrix.docset | gzip -nc9 --no-name > Matrix.tgz
